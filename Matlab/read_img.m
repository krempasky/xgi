function [ img_stack ] = read_img(dir_path,img_nb,file_base)

file_ext = 'h5';
cam_orientation = [ 1 0 1 ];
groupname =  '/entry/instrument/detector/data' ;
img_stack = zeros(1024,1024,length(img_nb),'double');

for i_imgnb = 1:length(img_nb)
    i_img = img_nb(i_imgnb);
    filename = [ dir_path.expdata  file_base sprintf('%04.0f.', i_img) file_ext ]
    % put the array into the preallocated stack
    if exist(filename,'file') == 2
         img = double(h5read(filename,groupname));
         if cam_orientation(1)
             img = permute(img, [ 2 1 3 ]);
         end
         if cam_orientation(2)
             img = flipdim(img, 2);
         end
         if cam_orientation(3)
             img = flipdim(img, 1);
         end
%          img_stack(:,:,i_img-img_nb(1)+1) = img;
         img_stack(:,:,i_imgnb) = img;
    else
        [ dir_path.expdata  file_base sprintf('%04.0f.', i_img) file_ext ]
        error('The file referred to in the above line does not exist')
    end
end
end
